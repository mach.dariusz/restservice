package com.sda.programowanie1.restapplication.service;

import com.sda.programowanie1.restapplication.fakeDB.RuntimeDB;
import com.sda.programowanie1.restapplication.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class EmployeeService {

    @Autowired
    private RuntimeDB db;

    public Optional<Employee> findById(int id) {
        return db.getDB().stream().filter(emp -> emp.getId() == id).findFirst();
    }

    public Optional<List<Employee>> getAll() {
        return Optional.of(db.getDB());
    }

    public Optional<Boolean> create(Employee employee) {
        return checkIfExist(employee.getId()) ? Optional.of(false) : Optional.of(db.getDB().add(employee));
    }

    public Optional<Employee> update(int id, Employee employee) {

        for (int i = 0; i < db.getDB().size(); i++) {
            if (db.getDB().get(i).getId() == id) {
                db.getDB().remove(i);
                db.getDB().add(i, employee);

                return Optional.of(employee);
            }
        }

        return null;
    }

    public Optional<Boolean> delete(int id) {
        if (checkIfExist(id)) {
            for (int i = 0; i < db.getDB().size(); i++) {
                if (db.getDB().get(i).getId() == id) {
                    Employee removedEmp = db.getDB().remove(i);
                }
            }
            return Optional.of(!checkIfExist(id));
        } else {
            return Optional.of(false);
        }
    }

    private boolean checkIfExist(int id) {
        return db.getDB().stream().anyMatch(emp -> emp.getId() == id);
    }
}
