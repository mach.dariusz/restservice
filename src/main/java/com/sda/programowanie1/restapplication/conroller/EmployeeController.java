package com.sda.programowanie1.restapplication.conroller;

import com.sda.programowanie1.restapplication.fakeDB.RuntimeDB;
import com.sda.programowanie1.restapplication.model.Employee;
import com.sda.programowanie1.restapplication.service.EmployeeService;
import org.apache.el.parser.BooleanNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/*  CRUD
        C -> CREATE
        R -> READ
        U -> UPDATE
        D -> DELETE
*/

// @CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/api")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping(path = "/employees")
    public ResponseEntity<List<Employee>> getAll() {
        return ResponseEntity.of(employeeService.getAll());
    }

    @GetMapping(path = "/employees/{id}")
    public ResponseEntity<Employee> get(@PathVariable("id") int id) {
        return ResponseEntity.of(employeeService.findById(id));
    }

    @PostMapping(path = "/employees")
    public ResponseEntity<Boolean> create(@RequestBody Employee employee) {
        return ResponseEntity.of(employeeService.create(employee));
    }

    @PutMapping(path = "/employees/{id}")
    public ResponseEntity<Employee> update(@PathVariable("id") int id, @RequestBody Employee employee) {

        return ResponseEntity.of(employeeService.update(id, employee));
    }

    @DeleteMapping(path = "/employees/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable("id") int id) {
        return ResponseEntity.of(employeeService.delete(id));
    }

}
