package com.sda.programowanie1.restapplication.fakeDB;

import com.sda.programowanie1.restapplication.model.Employee;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RuntimeDB {

    private final static List<Employee> EMPLOYEE_LIST = new ArrayList<>();

    public List<Employee> getDB() {
        return EMPLOYEE_LIST;
    }
}
